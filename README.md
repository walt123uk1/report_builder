
# Task

To create a "report builder" class in Laravel that can be accessed from anywhere in the application and returns data based on a string-based alias that references the type of report required. Data returned can be either a collection or array and the builder should be flexible enough to retrieve data via eloquent or directly from the database.

eg; We could call $data = $report->build()->get(); or $report->build()->export();

Where $report is an instance of our report builder class that has had an alias such as "Sessions" passed to it to define which report/data should be returned.

We will be looking at your interpretation of the brief and architectural decisions. Actual data does not necessarily need to be returned in the completed test, only the facility to return data-sets based on an alias is required.


# SOLUTION

## Class ReportBuilder

* I have created a class ReportBuilder in a new folder App\Classes.
* I have declared the namespace for Laravel.
* The class has a public  function "build" ("accessed from anywhere in the application").
* It accepts a string variable that indicates which data to retrieve and in what format.
* I've hard coded an array to save time.

**Please note I am aware of class aliases and eloquent aliases but I have assumed 'alias' in this case is a string variable that can be interpereted to give us table name and data type.

## Views

* I have created 2 views...
* Report Menu (report_menu.blade.php) where a user can select a report (BBC Hosts or Cars).
* Report Display (report_display.blade.php) that displays the returned data and has a return to menu button.

## Routes

* I have added routes to the web.php in the Routes folder.
* They are 'home' and 'report.
* Home is our menu.
* Report passes the 'report_alias' variable to our 'Report' function in the 'ReportController'. 

## Controller

* I have created a class 'ReportController'. 
* I have added a 'Use' statement so we can reference our external class 'ReportBuilder'.
* In the 'Report' function in the controller we use a new instance of report builder and set $data = $report->build($alias).
* The function 'build' returns data.
* We then pass this data to the Report Display blade.  

## If I had more time I would...

* Create some tables using migrations.
* Seed some data.
* Show example using eloquent to get data. 
* Make the array more dynamic instead of using field_1,2,3 I'd give them a name and format.
* Style the front end better.

* I've not really addressed $report->build()->get(); versus $report->build()->export();
* I assume it has something to do the format of the data ans how it is returned.
* I am unsure if ->export() is a function in the same way ->get() as a real world example I'd have asked for a further explanation.



