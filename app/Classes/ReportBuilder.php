<?php

namespace App\Classes;

class ReportBuilder {
    public function build($alias) {
        switch ($alias) {
        case "report_1":
            $content = [
                ['field_1' => 'Gary Lineker', 'field_2' => 'Host', 'field_3' => 'MotD'],
                ['field_1' => 'Alan Shearer', 'field_2' => 'Pundit', 'field_3' => 'MotD'],
                ['field_1' => 'Ian Wright', 'field_2' => 'Pundit', 'field_3' => 'MotD']
            ];
            //$content->toArray();
            break;
        case "report_2":
            $content = [
                ['field_1' => 'Ford', 'field_2' => 'Mustang', 'field_3' => '4.0'],
                ['field_1' => 'Mazda', 'field_2' => '326', 'field_3' => '2.0'],
                ['field_1' => 'BMW', 'field_2' => '5 Series', 'field_3' => '1.6']
            ];
            //$content->toArray();
            break;
        default:
            $content = [
                ['field_1' => 'Your data is not available'],
            ];
        }
        return $content;
    }
}