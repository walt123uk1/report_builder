<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Classes\ReportBuilder;

class ReportController extends Controller
{
     /**
     * Display a listing of the reports.
     *
     */
    public function index()
    {        
        return view('report_menu');
    }
    /**
    * Display a listing of the reports.
    *
    */
   public function report($alias)
   {        
    $report = new ReportBuilder();
    $data = $report->build($alias);
    //dd($data);
    return view('report_display', compact('data'));
   }
}
